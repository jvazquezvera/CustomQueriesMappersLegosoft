package com.mx.legosoft.jbpm.mappers;


import java.util.Map;
import org.dashbuilder.dataset.DataSet;
import org.jbpm.kie.services.impl.query.mapper.AbstractQueryMapper;
import org.jbpm.services.api.query.QueryResultMapper;

import com.mx.legosoft.jbpm.mappers.model.Count;


public class CountQueryMapper extends AbstractQueryMapper<Count> implements QueryResultMapper<Count> {

	private static final long serialVersionUID = 5935133069234696712L;
    /**
     * Dedicated for ServiceLoader to create instance, use <code>get()</code> method instead 
     */
    public CountQueryMapper() {
        super();
    }
     
    public static TaskPotOwnersQueryMapper get() {
        return new TaskPotOwnersQueryMapper();
    }
     
    @Override
    public Count map(Object result) {
    	Count count = new Count();
        if (result instanceof DataSet) {
            DataSet dataSetResult = (DataSet) result;
            
             
            if (dataSetResult != null) {
            	count.setCount(dataSetResult.getRowCount()); 
            }
             
            return count;
        }
         
        throw new IllegalArgumentException("Unsupported result for mapping " + result);
    }
   
     
    @Override
    public String getName() {
        return "count";
    }
 
    @Override
    public Class<?> getType() {
        return Integer.class;
    }
 
    @Override
    public QueryResultMapper<Count> forColumnMapping(Map<String, String> columnMapping) {
        return new CountQueryMapper();
    }

	@Override
	protected Count buildInstance(DataSet arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}
 
}