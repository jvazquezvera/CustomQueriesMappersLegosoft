package com.mx.legosoft.jbpm.mappers.model;

import org.kie.api.task.model.Status;
import org.kie.api.task.model.User;
import java.util.Date;

public interface TaskSummary {

	

    long getId();

    long getProcessInstanceId();

    String getName();

    String getSubject();

    String getDescription();

    Status getStatus();

    int getPriority();

    boolean isSkipable();

    User getActualOwner();

    User getCreatedBy();

    Date getCreatedOn();

    Date getActivationTime();

    Date getExpirationTime();

    String getProcessId();

    int getProcessSessionId();


	
	
}
